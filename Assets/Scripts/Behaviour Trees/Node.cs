namespace Behaviour_Trees
{
    
    public enum NodeState
    {
        Success,
        Failure,
        Running
    }
    
    [System.Serializable]
    public abstract class Node
    {
        private NodeState _nodeState;
        public NodeState NodeState
        {
            get => _nodeState;
            set => _nodeState = value;
        }

        public abstract NodeState Evaluate();
    }
    
    
}
