﻿using System.Collections.Generic;

namespace Behaviour_Trees
{
    public class Sequence : Node
    {
        private readonly List<Node> _nodes;
        
        public Sequence(List<Node> nodes)
        {
            _nodes = nodes;
        }
        
        public override NodeState Evaluate()
        {
            bool isAnyNodeRunning = false;
            foreach (Node node in _nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeState.Running:
                        isAnyNodeRunning = true;
                        break;
                    case NodeState.Success:
                        break;
                    case NodeState.Failure:
                        NodeState = NodeState.Failure;
                        return NodeState;
                }
            }
            NodeState = isAnyNodeRunning ? NodeState.Running : NodeState.Success;
            return NodeState;
        }
    }
}