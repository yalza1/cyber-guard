﻿namespace Behaviour_Trees
{
    public class Inventor : Node
    {
        private readonly Node _note;
        
        public Inventor(Node note)
        {
            _note = note;
        }
        public override NodeState Evaluate()
        {
            switch (_note.Evaluate())
            {
                case NodeState.Running:
                    NodeState = NodeState.Running;
                    break;
                case NodeState.Success:
                    NodeState = NodeState.Failure;
                    break;
                case NodeState.Failure:
                    NodeState = NodeState.Success;
                    break;
            }

            return NodeState;
        }
    }
}