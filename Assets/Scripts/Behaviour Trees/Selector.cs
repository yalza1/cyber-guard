﻿using System.Collections.Generic;

namespace Behaviour_Trees
{
    public class Selector : Node
    {
        private readonly List<Node> _nodes;
        
        public Selector(List<Node> nodes)
        {
            _nodes = nodes;
        }
        public override NodeState Evaluate()
        {
            foreach (Node node in _nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeState.Running:
                        NodeState = NodeState.Running;
                        return NodeState;
                    case NodeState.Success:
                        NodeState = NodeState.Success;
                        return NodeState;
                    case NodeState.Failure:
                        break;
                }
            }
            NodeState = NodeState.Failure;
            return NodeState;
        }
    }
}