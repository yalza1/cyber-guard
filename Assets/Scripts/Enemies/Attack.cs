﻿using Interfaces;
using UnityEngine;

namespace Enemies
{
    public class Attack : MonoBehaviour
    {
        public float damage = 20f;
        private void OnTriggerEnter(Collider other)
        {
            IDamageable damageable = other.GetComponentInParent<IDamageable>();
            if(damageable != null)
                damageable.TakeDamage(damage);
        }
    }
}