﻿
using Behaviour_Trees;
using Interfaces;
using Nodes;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
    public class EnemyAI : MonoBehaviour , IDamageable
    {
        private Node _root;
        
        #region Health
        [Header("Health")]
        [SerializeField] private float startingHealth = 100f;
        [SerializeField] private float lowHealthThreshold = 30f;
        [SerializeField] private float healthRestoreRate = 10f;
        private float _health;

        public float Health
        {
            get => _health;
            set => _health = Mathf.Clamp(value, 0f, startingHealth);
        }

        #endregion

        [SerializeField] private Transform playerTransform;
        [SerializeField] private Cover[] availableCovers;

        [SerializeField] private float chasingRange;
        [SerializeField] private float shootingRange;
        
        private Transform _bestCoverSpot;
        public Transform BestCoverSpot => _bestCoverSpot;
        
        private NavMeshAgent _agent;
        
        #region Unity Functions

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
        }

        private void Start()
        {
            Health = startingHealth;
            ConstructBehaviourTree();
            
        }
        
        private void ConstructBehaviourTree()
        {
            /*IsCoveredAvaliableNode isCoveredAvaliableNode = new IsCoveredAvaliableNode(availableCovers, playerTransform, this);
            GoToCoverNode goToCoverNode = new GoToCoverNode(_agent, this);
            HealthNode healthNode = new HealthNode(this, lowHealthThreshold);
            var transform1 = transform;
            IsCoveredNode isCoveredNode = new IsCoveredNode(playerTransform, transform1);
            /*ChaseNode chaseNode = new ChaseNode(playerTransform,_agent);#1#
            RangeNode chasingRangeNode = new RangeNode(transform1, playerTransform, chasingRange);
            RangeNode shootingRangeNode = new RangeNode(transform1, playerTransform, shootingRange);
            /*ShootNode shootNode = new ShootNode(_agent, this);#1#
            
            /*Sequence chaseSequence = new Sequence(new List<Node>{chasingRangeNode,chaseNode});#1#
            /*Sequence shootSequence = new Sequence(new List<Node>{shootingRangeNode,shootNode});#1#
            Sequence goToCoverSequence = new Sequence(new List<Node>{isCoveredAvaliableNode,goToCoverNode});
            
            /*Selector findCoverSelector = new Selector(new List<Node>{goToCoverSequence,chaseSequence});#1#
            /*Selector tryToTakeCoveSelector = new Selector(new List<Node>{isCoveredNode,findCoverSelector});#1#
            
            /*Sequence mainCoverSequence = new Sequence(new List<Node>{healthNode,tryToTakeCoveSelector});#1#
            
            /*_root = new Selector(new List<Node>{mainCoverSequence,shootSequence,chaseSequence});#1#*/
        }
        private void Update()
        {
            Health += healthRestoreRate * Time.deltaTime;
            _root.Evaluate();
        }
        #endregion
        
        public void SetBestCoverSpot(Transform bestCoverSpot)
        {
            _bestCoverSpot = bestCoverSpot;
        }
        
        public void TakeDamage(float damage)
        {
            throw new System.NotImplementedException();
        }
    }
}