﻿using System;
using System.Collections.Generic;
using Behaviour_Trees;
using Core;
using Interfaces;
using Items;
using Nodes;
using Player;
using UnityEngine;
using UnityEngine.UI;


namespace Enemies
{
    [Serializable]
    public class ShootProperties
    {
        public GameObject muzzleFlash;
        public GameObject explosionFx;
        public GameObject bulletPrefab;
        public Transform bulletSpawnPoint;
        public float timeBetweenShots;
        public Vector3 targetOffset;
        public AudioClip shootSound;
    }
    public class Turret : MonoBehaviour ,IDamageable , IShootable , IDropItemWhenDie
    {
        
        public ShootProperties shootProperties;

        [Header("Generals")] [SerializeField] private Transform head;
        public Transform playerTransform;
        private Slider _healthBar;
        private Node _root;
        private AudioSource _audioSource;
        
        [Header("Stats")]
        [SerializeField] private float maxHealth = 100f;
        [SerializeField] private float shootingRange = 100f;
        
        
        private float _health = 100f;

        private void Awake()
        {
            _healthBar = GetComponentInChildren<Slider>();
            playerTransform = Character.Instant.transform;
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            _health = maxHealth;
            ConstructBehaviourTree();
        }
        
        private void ConstructBehaviourTree()
        {
            RangeNode shootRangeNode = new RangeNode(head, playerTransform, shootingRange);
            ShootNode shootNode = new ShootNode(head,playerTransform, shootProperties);

            _root = new Sequence(new List<Node>
            {
                shootRangeNode,
                shootNode
            });
        }

        private void Update()
        {
            _root.Evaluate();
        }

        public void TakeDamage(float damage)
        {
            _health -= damage;
            _healthBar.value = _health / maxHealth;
            if (_health <= 0f)
            {
                Die();
            }
        }

        private void Die()
        {
            Transform transform1;
            
            ObjectPooling.Instant.GetGameObject(shootProperties.explosionFx, (transform1 = transform).position, transform1.rotation)
                .SetActive(true);
            Destroy(gameObject,0.5f);
            DropItem();
        }

        public void Shoot()
        {
            _audioSource.PlayOneShot(shootProperties.shootSound);
            _audioSource.loop = false;
        }

        public void DropItem()
        {
            var transform1 = transform;
            ItemManager.Instant.DropRandomItem(transform1.position + new Vector3(0,5,0),transform1.rotation);
        }
    }
}