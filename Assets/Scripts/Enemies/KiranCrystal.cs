﻿using System.Collections;
using System.Collections.Generic;
using Behaviour_Trees;
using Core;
using Interfaces;
using Nodes;
using Player;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
namespace Enemies
{
    public class KiranCrystal : MonoBehaviour , IDamageable
    {
        private NavMeshAgent _agent;
        private Animator _animator;
        private Transform _playerTransform;
        private Slider _healthBar;
        [SerializeField] private float maxHealth = 100f;
        [SerializeField] private float speed = 5f;
        [SerializeField] private float attackRange = 5f;
        [SerializeField] private float chaseRange = 100f;
        
        private float _health = 100f;
        private Node _root;
        private static readonly int Die1 = Animator.StringToHash("Die");


        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _healthBar = GetComponentInChildren<Slider>();
            _playerTransform = Character.Instant.transform;
            _animator = GetComponent<Animator>();
        }
        private void Start()
        {
            _agent.speed = speed;
            _health = maxHealth;
            ConstructBehaviourTree();
        }

        private void ConstructBehaviourTree()
        {
            var transform1 = transform;
            RangeNode attackRangeNode = new RangeNode(transform1, _playerTransform, attackRange);
            RangeNode chaseRangeNode = new RangeNode(transform1, _playerTransform, chaseRange);
            
            ChaseNode chaseNode = new ChaseNode(_playerTransform, _agent,true);
            MeleeAttackNode meleeAttackNode = new MeleeAttackNode(transform1, _playerTransform, _agent);
            
            Sequence chaseSequence = new Sequence(new List<Node>
            {
                chaseRangeNode,
                chaseNode
            });
            
            Sequence attackSequence = new Sequence(new List<Node>
            {
                attackRangeNode,
                meleeAttackNode
            });
            
            _root = new Selector(new List<Node>
            {
                attackSequence,
                chaseSequence
            });
        }
        
      

        private void Update()
        {
            _root.Evaluate();
        }

        public void TakeDamage(float damage)
        {
            _health -= damage;
                _healthBar.value = _health / maxHealth;
            if (_health <= 0)
            {
                _health = 0;
                Die();
            }
        }

        private void Die()
        {
            GameManager.Instant.isWin = true;
            GameManager.Instant.bossDead = true;
            GameManager.Instant.GameOver();
            StartCoroutine(IDie());
        }

        IEnumerator IDie()
        {
            _agent.isStopped = true;
            _animator.SetTrigger(Die1);
            yield return new WaitForSeconds(5f);
            Time.timeScale = 0;
            Observer.Instant.NotifyObservers(Constant.GameOver);
        }
    }
}