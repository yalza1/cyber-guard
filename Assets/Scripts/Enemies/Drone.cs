﻿
using System.Collections.Generic;
using Behaviour_Trees;
using Core;
using Interfaces;
using Items;
using Nodes;
using Player;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Enemies
{
    public class Drone : MonoBehaviour, IDamageable , IShootable , IDropItemWhenDie
    {

        public ShootProperties shootProperties;
        private NavMeshAgent _agent;

        [Header("Generals")]
        public Transform playerTransform;
        private Slider _healthBar;
        private Node _root;
        private AudioSource _audioSource;

        [Header("Stats")] [SerializeField] private float maxHealth = 100f;
        [SerializeField] private float shootingRange = 100f;
        [SerializeField] private float chaseRange = 100f;
        [SerializeField] private float speed = 5f;

        private float _health = 100f;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _healthBar = GetComponentInChildren<Slider>();
            playerTransform = Character.Instant.transform;
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            _agent.speed = speed;
            _health = maxHealth;
            _audioSource.volume = 0.1f;
            ConstructBehaviourTree();
        }

        private void Update()
        {
            _root.Evaluate();
        }

        private void ConstructBehaviourTree()
        {
            var transform1 = transform;
            RangeNode shootRangeNode = new RangeNode(transform1, playerTransform, shootingRange);
            RangeNode chaseRangeNode = new RangeNode(transform1, playerTransform, chaseRange);
            
            ChaseNode chaseNode = new ChaseNode(playerTransform, _agent,false);
            ShootNode shootNode = new ShootNode(transform1, playerTransform, shootProperties);
            
            Sequence chaseSequence = new Sequence(new List<Node>
            {
                chaseRangeNode,
                chaseNode
            });
            
            Sequence shootSequence = new Sequence(new List<Node>
            {
                shootRangeNode,
                shootNode
            });
            
            _root = new Selector(new List<Node>
            {
                shootSequence,
                chaseSequence,
            });
        }

        public void TakeDamage(float damage)
        {
            _health -= damage;
            _healthBar.value = _health / maxHealth;
            if (_health <= 0f)
            {
                Die();
            }
        }

        private void Die()
        {
            Transform transform1;
            ObjectPooling.Instant.GetGameObject(shootProperties.explosionFx, (transform1 = transform).position, transform1.rotation)
                .SetActive(true);
            Destroy(gameObject,1f);
            DropItem();
        }

        public void Shoot()
        {
            _audioSource.PlayOneShot(shootProperties.shootSound);
            _audioSource.loop = false;
        }

        public void DropItem()
        {
            var transform1 = transform;
            ItemManager.Instant.DropRandomItem(transform1.position + new Vector3(0,5,0),transform1.rotation);
        }
    }
}