﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Utils
{
    public class TimedObjectDeactiver : MonoBehaviour
    {
        [SerializeField] private float lifeTime = 1f;

        private void OnEnable()
        {
            Invoke(nameof(Deactive), lifeTime);
        }

        private void Deactive()
        {
            gameObject.SetActive(false);
        }
    }
}