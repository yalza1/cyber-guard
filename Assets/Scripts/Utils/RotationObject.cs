﻿using UnityEngine;

namespace Utils
{
    public class RotationObject : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed = 1f;
        [SerializeField] private Vector3 rotationAxis = Vector3.up;
        private void Update()
        {
            transform.Rotate(rotationAxis, rotationSpeed * Time.deltaTime);
        }
    }
}