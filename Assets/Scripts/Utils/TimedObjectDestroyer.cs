﻿using UnityEngine;

namespace Utils
{
    public class TimedObjectDestroyer : MonoBehaviour
    {
        [SerializeField] private float lifeTime = 1f;

        private void Start()
        {
            Invoke(nameof(Deactive), lifeTime);
        }

        private void Deactive()
        {
            Destroy(gameObject);
        }
    }
}