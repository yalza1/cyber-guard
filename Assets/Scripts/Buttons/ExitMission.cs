﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Buttons
{
    public class ExitMission : MonoBehaviour
    {
        public void OnClick()
        {
            SceneManager.LoadScene("LobbyRoomScene");
        }
    }
}