﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Weapon
{
    public class GunItem : MonoBehaviour
    {
        public GunData gunData;
        public GameObject parent;
        
        
        
        public void Onclick()
        {
            GameObject model = Instantiate(gunData.prefab, parent.transform);
            Image image = model.GetComponent<Image>();
            image.color = new Color(0, 0, 0, 0);
            image.raycastTarget = false;
            GameObject wp = parent.transform.GetChild(0).gameObject;
            if(wp)Destroy(wp);
            
            model.transform.localPosition = Vector3.zero;
            model.transform.localRotation = Quaternion.identity;
            model.transform.localScale = Vector3.one * 2;
        }
    }
}