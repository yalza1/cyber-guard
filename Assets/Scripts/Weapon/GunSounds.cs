﻿using System;
using UnityEngine;

namespace Weapon
{
    public enum SoundType
    {
        Shoot,
        Reload,
        Empty,
        Equip,
        Unequip
    }
    public class GunSounds : MonoBehaviour
    {
        [SerializeField] private AudioClip shootSound;
        [SerializeField] private AudioClip reloadSound;
        [SerializeField] private AudioClip emptySound;
        [SerializeField] private AudioClip equipSound;
        [SerializeField] private AudioClip unequipSound;
        
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        
        public void PlaySound(SoundType soundType)
        {
            switch (soundType)
            {
                case SoundType.Shoot:
                    _audioSource.PlayOneShot(shootSound);
                    break;
                case SoundType.Reload:
                    _audioSource.PlayOneShot(reloadSound);
                    break;
                case SoundType.Empty:
                    _audioSource.PlayOneShot(emptySound);
                    break;
                case SoundType.Equip:
                    _audioSource.PlayOneShot(equipSound);
                    break;
                case SoundType.Unequip:
                    _audioSource.PlayOneShot(unequipSound);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(soundType), soundType, null);
            }
        }
    }
}