﻿using System;
using Core;
using Interfaces;
using UnityEngine;
namespace Weapon
{
    public class SwordAttack : MonoBehaviour
    {
        [SerializeField] private float damage = 10f;
        [SerializeField] private GameObject impactEffect;
        
        private SwordSounds _swordSounds;

        private void Awake()
        {
            _swordSounds = GetComponent<SwordSounds>();
        }
        

        private void OnCollisionEnter(Collision other)
        {
            IDamageable damageable = other.gameObject.GetComponentInParent<IDamageable>();
            if (damageable != null)
            {
                damageable.TakeDamage(damage);
            }
            
            _swordSounds.PlaySound(SwordSoundType.Hit);
            Vector3 contactPoint = other.GetContact(0).point;
            
            ObjectPooling.Instant.GetGameObject(impactEffect, contactPoint, Quaternion.identity).SetActive(true);
        }
    }
}