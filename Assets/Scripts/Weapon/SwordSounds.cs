﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Weapon
{
    public enum SwordSoundType
    {
        Swing,
        Hit,
        Draw,
        Unequip
    }
    public class SwordSounds : MonoBehaviour
    {
       
        
        [SerializeField] private AudioClip swingSound;
        [SerializeField] private AudioClip hitSound;
        [SerializeField] private AudioClip drawSound;
        [SerializeField] private AudioClip unequipSound;
        
        private AudioSource _audioSource;
        
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        public void PlaySound(SwordSoundType soundType)
        {
            switch (soundType)
            {
                case SwordSoundType.Swing:
                    _audioSource.PlayOneShot(swingSound);
                    break;
                case SwordSoundType.Hit:
                    _audioSource.PlayOneShot(hitSound);
                    break;
                case SwordSoundType.Draw:
                    _audioSource.PlayOneShot(drawSound);
                    break;
                case SwordSoundType.Unequip:
                    _audioSource.PlayOneShot(unequipSound);
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException(nameof(soundType), soundType, null);
            }
        }
    }
}