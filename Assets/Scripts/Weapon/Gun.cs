using System;
using Core;
using Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Weapon
{
    public class Gun : MonoBehaviour
    {
        [SerializeField] private Transform muzzle;
        [SerializeField] private GameObject muzzleFlash;
        [SerializeField] private GameObject impactEffect;
        [SerializeField] private float fireRate = 0.5f;
        [SerializeField] private float damage = 10f;
        [SerializeField] private float accuracy = 0.2f;
        [SerializeField] private int clipSize = 30;
        private Vector2 _acuracy = new Vector2(1f, 1f);
        private int _currentAmmo;
        
        private GunSounds _gunSounds;
        
        
        private Transform _cameraTransform;
        
        

        [SerializeField] private int maxAmmo = 1000;
        
        
        public float FireRate => fireRate;

        private void Awake()
        {
            _gunSounds = GetComponent<GunSounds>();
        }

        private void Start()
        {
            _cameraTransform = Camera.main.transform;
            _currentAmmo = clipSize;
            _acuracy = new Vector2(accuracy, accuracy);
            Observer.Instant.NotifyObservers(Constant.UpdateAmmo,(_currentAmmo,clipSize));
            Observer.Instant.NotifyObservers(Constant.UpdateMaxAmmo, (maxAmmo));
            
        }
        

        public void Fire()
        {
            _gunSounds.PlaySound(SoundType.Shoot);
            
            Observer.Instant.NotifyObservers(Constant.AcuracyCrosshair);
            _currentAmmo--;
            Observer.Instant.NotifyObservers(Constant.UpdateAmmo,(_currentAmmo,clipSize));
            
            var transform1 = _cameraTransform;
            var accuracy = new Vector3(Random.Range(-_acuracy.x, _acuracy.x), Random.Range(-_acuracy.y, _acuracy.y), 0) + transform1.position;
            Ray ray = new Ray(accuracy, transform1.forward);
            if(Physics.Raycast(ray.origin, ray.direction, out var hit,maxDistance:1000))
            {
                var damageable = hit.collider.GetComponentInParent<IDamageable>();
                if (damageable != null)
                {
                    damageable.TakeDamage(damage);
                }
                
                var impact = ObjectPooling.Instant.GetGameObject(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                impact.SetActive(true);
                
                var flash = ObjectPooling.Instant.GetGameObject(muzzleFlash, muzzle.position, muzzle.rotation);
                flash.transform.SetParent(muzzle);
                flash.SetActive(true);
            }
        }

        public bool CanFire()
        {
            if (_currentAmmo == 0) return false;
            return true;
        }
        
        public bool CanReload()
        {
            if (_currentAmmo == clipSize) return false;
            if (maxAmmo < clipSize) return false;
            return true;
        }
        
        public void Reload()
        {
            _gunSounds.PlaySound(SoundType.Reload);
            _currentAmmo = clipSize;
            Observer.Instant.NotifyObservers(Constant.UpdateAmmo,(_currentAmmo,clipSize));
            Observer.Instant.NotifyObservers(Constant.UpdateMaxAmmo,maxAmmo);
            maxAmmo -= clipSize;
        }
        
    }
}
