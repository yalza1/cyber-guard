﻿using UnityEngine;

namespace Weapon
{
    [CreateAssetMenu(fileName = "Sword Data", menuName = "Weapon Data/Sword Data", order = 0)]
    public class SwordData : ScriptableObject
    {
        public bool isOwned;
        public int id;
        public new string name;
        [TextArea(4,4)]
        public string description;
        public int rank;
        public Sprite icon;
        public GameObject prefab;
        
        [Space]
        [Header("Parameters")]
        public float damage;
        public float durability;
        public float attackSpeed;
        public float weight;
        public float length;
        public float range;
    }
}