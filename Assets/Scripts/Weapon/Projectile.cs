﻿using System;
using Core;
using Interfaces;
using UnityEngine;

namespace Weapon
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float speed = 10f;
        [SerializeField] private float damage = 10f;
        [SerializeField] private GameObject impactEffect;

        private void Update()
        {
            transform.Translate(Vector3.forward * (speed * Time.deltaTime));
        }
        
        private void OnTriggerEnter(Collider other)
        {
            var damageable = other.GetComponentInParent<IDamageable>();
            if (damageable != null)
            {
                damageable.TakeDamage(damage);
            }

            Transform transform1;
            ObjectPooling.Instant.GetGameObject(impactEffect, (transform1 = transform).position, transform1.rotation).SetActive(true);
            gameObject.SetActive(false);
        }
    }
}