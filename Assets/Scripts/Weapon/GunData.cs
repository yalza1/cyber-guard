﻿using UnityEngine;

namespace Weapon
{
    [CreateAssetMenu(fileName = "Gun Data", menuName = "Weapon Data/Gun Data", order = 0)]
    public class GunData : ScriptableObject
    {
        public bool isOwned;
        public int id;
        public new string name;
        [TextArea(4,4)]
        public string description;
        public int rank;
        public Sprite icon;
        public GameObject prefab;
        [Space]
        [Header("Parameters")]
        public float damage;
        public int magazineSize;
        public float reloadTime;
        public float fireRate;
        public float recoil;
        public float falloffRange;
    }
}