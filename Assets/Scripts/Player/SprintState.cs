﻿using UnityEngine;

namespace Player
{
    public class SprintState : State
    {
        private float _gravity;

        private bool _grounded;
        private bool _sprint;
        private float _playerSpeed;
        private bool _sprintJump;
        private bool _sprintSlide;
        
        public SprintState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            Character.AudioSource.PlayOneShot(Character.walkSound);
            Character.AudioSource.pitch = 1.5f;
            Character.AudioSource.volume = 1f;
            
            _sprint = false;

            _sprintJump = false;
            Input = Vector2.zero;
            Velocity = Vector3.zero;
            GravityVelocity.y = 0f;
            _sprintSlide = false;
            _playerSpeed = Character.sprintSpeed;
            _grounded = Character.Grounded;
            _gravity = Character.Gravity;
        }

        public override void HandleInput()
        {
            base.HandleInput();
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            
            Velocity = Velocity.x * Character.CameraTransform.right.normalized + Velocity.z * Character.CameraTransform.forward.normalized;
            Velocity.y = 0;
            
            if (SprintAction.triggered || Input.magnitude < 0.1f)
            {
                _sprint = false;
            }
            else
            {
                _sprint = true;
            }
            
            if(JumpAction.triggered)
            {
                _sprintJump = true;
            }
            if(CrouchAction.triggered)
            {
                _sprintSlide = true;
            }
            
            if(Mathf.Abs(Input.x) > 0.1f)
            {
                _sprint = false;
            }
            
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (_sprint)
            {
                Character.Animator.SetFloat(Speed, Input.magnitude + 1f, Character.speedDampTime, Time.deltaTime);
            }
            else 
            {
                StateMachine.ChangeState(Character.StandingState);
            }

            if (_sprintJump)
            {
                StateMachine.ChangeState(Character.SprintJumpState);
            }

            if (_sprintSlide)
            {
                StateMachine.ChangeState(Character.SprintSlideState);
                
            }
            
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
            if(_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = 0f;
            }
            
            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }
        
        public override void Exit()
        {
            base.Exit();
            Character.AudioSource.Stop();
        }
    }
}