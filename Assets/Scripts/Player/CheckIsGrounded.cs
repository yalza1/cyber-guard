﻿using System;
using UnityEngine;

namespace Player
{
    public class CheckIsGrounded : MonoBehaviour
    {
        public bool Check()
        {
            RaycastHit ray;
            Debug.DrawRay(transform.position, Vector3.down * 1.1f, Color.red);
            if (Physics.Raycast(transform.position, Vector3.down, out ray, 1.1f))
            {
                return true;
            }

            return false;
        }
    }
}