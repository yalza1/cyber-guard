﻿using System.Collections;
using UnityEngine;

namespace Player
{
    public class JumpingState : State
    {
        private bool _grounded;
        private float _gravity;
        private float _jumpHeight;
        private float _playerSpeed;
        
        

        public JumpingState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            Character.AudioSource.PlayOneShot(Character.jumpSound);
            Character.AudioSource.pitch = 0.7f;
            Character.AudioSource.volume = 1f;
            
            _grounded = false;
            _gravity = Character.Gravity;
            _jumpHeight = Character.JumpHeight;
            _playerSpeed = Character.playerSpeed;
            GravityVelocity.y = 0f;
            Character.Animator.SetFloat(Speed, 0f);
            Character.Animator.SetTrigger(Jumping);
            Jump();
        }

        public override void HandleInput()
        {
            base.HandleInput();

            Input = MoveAction.ReadValue<Vector2>();
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8 && !Character.Animator.IsInTransition(0))
            {
                StateMachine.ChangeState(Character.LandingState);
            }
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            if(!_grounded)
            {
                Velocity = Character.PlayerVelocity;
                var right = Character.CameraTransform.right;
                var forward = Character.CameraTransform.forward;
                Velocity = Velocity.x * right.normalized + Velocity.z * forward.normalized;
                Velocity.y = 0f;
                Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
            }
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
        }
        
        public override void Exit()
        {
            base.Exit();
            Character.AudioSource.Stop();
        }
        
        private void Jump()
        {
            GravityVelocity.y += Mathf.Sqrt(_jumpHeight * -10f * _gravity);
        }
    }
}