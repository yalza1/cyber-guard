﻿using UnityEngine;

namespace Player
{
    public class SprintJumpState : State
    {
        private float _timePassed = 0f;
        private float _jumpTime;
        private float _jumpHeight;
        private float _gravity;
        private bool _grounded;
        private Vector3 _airVelocity;
        private float _playerSpeed;

        public SprintJumpState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            Character.AudioSource.PlayOneShot(Character.jumpSound);
            Character.AudioSource.pitch = 0.9f;
            Character.AudioSource.volume = 1f;
            
            _timePassed = 0f;
            _grounded = false;
            _gravity = Character.Gravity;
            _jumpHeight = Character.JumpHeight;
            _playerSpeed = Character.playerSpeed;
            GravityVelocity.y = 0f;
            _jumpTime = 1f;
            Character.Animator.SetTrigger(SprintJump);
            Jump();
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (_timePassed >= _jumpTime)
            {
                StateMachine.ChangeState(Character.SprintState);
            }
            _timePassed += Time.deltaTime;
        }
        
        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            if(!_grounded)
            {
                Velocity = Character.PlayerVelocity;
                var right = Character.CameraTransform.right;
                var forward = Character.CameraTransform.forward;
                Velocity = Velocity.x * right.normalized + Velocity.z * forward.normalized;
                Velocity.y = 0f;
                
            }
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }

        public override void Exit()
        {
            base.Exit();
            Character.AudioSource.Stop();
            Character.Animator.ResetTrigger(SprintJump);
        }
        
        private void Jump()
        {
            GravityVelocity.y += Mathf.Sqrt(_jumpHeight * -5f * _gravity);
        }
    }
}