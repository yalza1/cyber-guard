﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class State
    {
        protected static readonly int Speed = Animator.StringToHash("speed");
        protected static readonly int Jumping = Animator.StringToHash("jump");
        protected static readonly int Land = Animator.StringToHash("land");
        protected static readonly int Move = Animator.StringToHash("move");
        protected static readonly int Crouch = Animator.StringToHash("crouch");
        protected static readonly int SprintJump = Animator.StringToHash("sprintJump");
        protected static readonly int DrawWeapon = Animator.StringToHash("drawWeapon");
        protected static readonly int SheetWeapon = Animator.StringToHash("sheetWeapon");
        protected static readonly int SprintSlide = Animator.StringToHash("sprintSlide");
        protected static readonly int MeleeAttack = Animator.StringToHash("meleeAttack");
        protected static readonly int MeleeAttackCombo = Animator.StringToHash("meleeAttackCombo");
        protected static readonly int Aim = Animator.StringToHash("aim");
        protected static readonly int Fire = Animator.StringToHash("fire");
        protected static readonly int Reload = Animator.StringToHash("reload");
        protected static readonly int Die = Animator.StringToHash("die");

        protected Character Character;
        protected StateMachine StateMachine;
        
        protected Vector3 GravityVelocity;
        protected Vector3 Velocity;
        protected Vector3 Input;

        protected readonly InputAction MoveAction;
        protected readonly InputAction JumpAction;
        protected readonly InputAction CrouchAction;
        protected readonly InputAction SprintAction;
        protected readonly InputAction DrawSwordAction;
        protected readonly InputAction DrawGunAction;
        protected readonly InputAction AttackAction;
        protected readonly InputAction AttackSpecialAction;
        protected readonly InputAction ReloadAction;
        

        protected State(Character character, StateMachine stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
            
            MoveAction = character.PlayerInput.actions["Move"];
            JumpAction = character.PlayerInput.actions["Jump"];
            CrouchAction = character.PlayerInput.actions["Crouch"];
            SprintAction = character.PlayerInput.actions["Sprint"];
            DrawSwordAction = character.PlayerInput.actions["DrawSword"];
            DrawGunAction = character.PlayerInput.actions["DrawGun"];
            AttackAction = character.PlayerInput.actions["Attack"];
            AttackSpecialAction = character.PlayerInput.actions["AttackSpecial"];
            ReloadAction = character.PlayerInput.actions["Reload"];
            
        }
        
        public virtual void Enter()
        {
            string scriptName = GetType().Name;
            Debug.Log("Script Name: " + scriptName);
        }
        
        public virtual void HandleInput()
        {
            
        }
        
        public virtual void LogicUpdate()
        {
            
        }
        
        public virtual void PhysicsUpdate()
        {
            
        }
        
        public virtual void Exit()
        {
            
        }
        
        
    }
}