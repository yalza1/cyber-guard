﻿using UnityEngine;

namespace Player
{
    public class CrouchingState : State
    {
        private float _playerSpeed;
        private bool _crouchHeld;
        private bool _grounded;
        private float _gravity;
        

        public CrouchingState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            Character.cameraPivot.GetComponent<Animator>().SetBool("crouch", true);
            
            Character.AudioSource.PlayOneShot(Character.walkSound);
            Character.AudioSource.pitch = 0.7f;
            Character.AudioSource.volume = 1f;
            
            Character.Animator.SetTrigger(Crouch);
            _crouchHeld = false;
            GravityVelocity.y = 0f;
            _playerSpeed = Character.crouchSpeed;
            _grounded = Character.Grounded;
            _gravity = Character.Gravity;
            
        }

        public override void HandleInput()
        {
            base.HandleInput();
            if (CrouchAction.triggered)
            {
                _crouchHeld = true;
            }
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            
            Velocity = Velocity.x * Character.CameraTransform.right.normalized + Velocity.z * Character.CameraTransform.forward.normalized;
            Velocity.y = 0;
        }
        
        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if(Input.magnitude < 0.1f)
            {
                Character.AudioSource.volume = 0f;
            }
            else
            {
                Character.AudioSource.volume = 1f;
            }
            
            Character.Animator.SetFloat(Speed, Input.magnitude,Character.speedDampTime, Time.deltaTime);
            if (_crouchHeld)
            {
                StateMachine.ChangeState(Character.StandingState);
            }
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
            if(_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = 0f;
            }
            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
            
        }


        public override void Exit()
        {
            base.Exit();
            Character.cameraPivot.GetComponent<Animator>().SetBool("crouch", false);
            GravityVelocity.y = 0f;
            Character.PlayerVelocity = new Vector3(Input.x, 0f, Input.y);
            Character.AudioSource.Stop();
        }
        
        
        
    }
}