﻿using System;
using UnityEngine;

namespace Player
{
    public class StandingState : State
    {
        float _gravityValue;
        bool _jump;
        bool _crouch;
        bool _grounded;
        bool _sprint;
        private bool _punch;
        float _playerSpeed;
        private float _targetWeight;


        private bool _drawSword;
        private bool _drawingWeapon;
        private bool _drawGun;
        private bool _aim;
        private bool _fire;
        private bool _reload;


        private float targetWeight;
        public StandingState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            _targetWeight = Character.Animator.GetLayerWeight(1);
            _jump = false;
            _crouch = false;
            _sprint = false;
            targetWeight = 0f;
            _aim = false;
            _fire = false;
            _reload = false;

            Input = Vector2.zero;
            Velocity = Vector3.zero;
            GravityVelocity.y = 0f;
            _drawSword = false;
            _drawGun = false;
            _playerSpeed = Character.playerSpeed;
            _grounded = Character.Grounded;
            _gravityValue = Character.Gravity;
            
            Character.Animator.SetTrigger(Move);
            
            Character.AudioSource.PlayOneShot(Character.walkSound);
            Character.AudioSource.pitch = 0.8f;
        }

        public override void HandleInput()
        {
            base.HandleInput();
            
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            var transform = Camera.main.transform;
            Velocity = Velocity.x * transform.right.normalized + Velocity.z * transform.forward.normalized;
            Velocity.y = 0;

            if (JumpAction.triggered)
            {
                _jump = true;
            }

            if (CrouchAction.triggered)
            {
                _crouch = true;
            }

            if (SprintAction.triggered && Input.y > 0.1f)
            {
                _sprint = true;
            }

            if (DrawSwordAction.triggered)
            {
                _drawSword = true;
            }

            if (DrawGunAction.triggered)
            {
                _drawGun = true;
            }
            
            if (ReloadAction.triggered && Character.weaponType == WeaponType.Gun && Character.Gun.CanReload())
            {
                _reload = true;
            }

            if (AttackAction.triggered && Character.weaponType != WeaponType.Gun)
            {
                StateMachine.ChangeState(Character.MeleeAttack);
            }

            if (AttackSpecialAction.triggered && Character.weaponType != WeaponType.Gun)
            {
                StateMachine.ChangeState(Character.MeleeComboState);
            }

            if (AttackSpecialAction.triggered && Character.weaponType == WeaponType.Gun)
            {
                _aim = true;
            }

            if (UnityEngine.Input.GetMouseButton(0) && Character.weaponType == WeaponType.Gun)
            {
                if(Character.Gun.CanFire()){
                    _fire = true;
                }
                else
                {
                    _reload = true;
                }
            }
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            
            if(Input.magnitude < 0.1f)
            {
                Character.AudioSource.volume = 0;
            }
            else
            {
                Character.AudioSource.volume = 1;
            }
            
            if (Character.weaponType == WeaponType.Gun)
            {
                if (targetWeight < 0.05)
                {
                    targetWeight = Mathf.MoveTowards(targetWeight,1,Time.deltaTime * 0.05f);
                }
                else
                {
                    targetWeight = Mathf.MoveTowards(targetWeight,1,Time.deltaTime * 3);
                }
                Character.Animator.SetLayerWeight(1,targetWeight);
            }

            Character.Animator.SetFloat(Speed, Input.magnitude, Character.speedDampTime, Time.deltaTime);
            Character.Animator.SetFloat("xAxis", Input.x, Character.speedDampTime, Time.deltaTime);

            if (_jump)
            {
                StateMachine.ChangeState(Character.JumpingState);
            }

            if (_crouch)
            {
                StateMachine.ChangeState(Character.CrouchingState);
            }

            if (_sprint)
            {
                StateMachine.ChangeState(Character.SprintState);
            }

            if (_reload)
            {
                StateMachine.ChangeState(Character.ReloadState);
            }

            if (_drawSword)
            {
                if (Character.weaponType == WeaponType.Gun)
                {
                    Character.EquipmentSystem.SheathWeapon();
                    Character.Animator.SetLayerWeight(1,0);
                    _drawingWeapon = false;
                }
                if (!_drawingWeapon)
                {

                    Draw(WeaponType.Sword);

                }
                else
                {
                    Sheath();
                }

                _drawSword = false;
            }

            if (_drawGun)
            {
                targetWeight = 0;
                if (Character.weaponType == WeaponType.Sword)
                {
                    Character.EquipmentSystem.SheathWeapon();
                    
                    _drawingWeapon = false;
                }

                if (!_drawingWeapon)
                {

                    Draw(WeaponType.Gun);
                }
                else
                {
                    Sheath();
                }

                _drawGun = false;
            }
            

            void Draw(WeaponType weaponType)
            {
                _drawingWeapon = true;
                Character.Animator.SetTrigger(DrawWeapon);
                Character.weaponType = weaponType;
            }

            void Sheath()
            {
                _drawingWeapon = false;
                Character.Animator.SetLayerWeight(1,0);
                Character.Animator.SetTrigger(SheetWeapon);
                Character.weaponType = WeaponType.None;
            }
            
            if(_aim)
            {
                StateMachine.ChangeState(Character.AimState);
            }
            
            if(_fire)
            {
                StateMachine.ChangeState(Character.FireState);
            }
            
            if(StateMachine.PreviousState == Character.AimState || StateMachine.PreviousState == Character.FireState || StateMachine.PreviousState == Character.ReloadState)
            {
                _targetWeight = Mathf.MoveTowards(_targetWeight,0,Time.deltaTime * 3);
                Character.Animator.SetLayerWeight(1,_targetWeight);
            }
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();

            GravityVelocity.y += _gravityValue * Time.deltaTime;
            _grounded = Character.Grounded;

            if (_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = -0f;
            }

            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }

        public override void Exit()
        {
            base.Exit();
            Character.AudioSource.Stop();
            Character.Animator.SetLayerWeight(1, 0f);
            Character.Animator.ResetTrigger(Move);
            GravityVelocity.y = 0;
            Character.PlayerVelocity = new Vector3(Input.x, 0, Input.y);

            if (Velocity.sqrMagnitude > 0)
            {
                Character.transform.rotation = Quaternion.LookRotation(Velocity);
            }
        }
    }
}