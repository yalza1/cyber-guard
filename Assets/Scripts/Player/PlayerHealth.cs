﻿using System;
using Core;
using Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class PlayerHealth : MonoBehaviour, IDamageable
    {
        Character _character;
        
        [SerializeField] private float maxHealth = 100f;
        private float _health = 100f;
        
        [SerializeField] private float maxShield = 100f;
        private float _shield = 100f;

        private void Awake()
        {
            _character = GetComponent<Character>();
        }

        private void Start()
        {
            _shield = maxShield;
            _health = maxHealth;
            Observer.Instant.NotifyObservers(Constant.UpdateHealthBar,(_health,maxHealth));
            Observer.Instant.NotifyObservers(Constant.UpdateShieldBar,(_shield,maxShield));
        }

        public void TakeDamage(float damage)
        {
            if (_character.isDead) return;
            if (_shield == 0f)
            {
                _health -= damage;
                
                if (_health <= 0f)
                {
                    _health = 0f;
                    Observer.Instant.NotifyObservers(Constant.UpdateHealthBar,(_health,maxHealth));
                    Die();
                }
                Observer.Instant.NotifyObservers(Constant.UpdateHealthBar,(_health,maxHealth));
            }
            else if (_shield > 0f)
            {
                _shield -= damage / 2;
                if (_shield <= 0f)
                {
                    _shield = 0f;
                }
                Observer.Instant.NotifyObservers(Constant.UpdateShieldBar,(_shield,maxShield));
            }
           
        }

        private void Die()
        {

            var list = GetComponentsInChildren<MouseRotation>();
            foreach (var mouseRotation in list)
            {
                mouseRotation.enabled = false;
            }
            _character.MovementStateMachine.ChangeState(_character.DyingState);
            GameManager.Instant.GameOver();
        }
    }
}