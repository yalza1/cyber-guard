﻿using UnityEngine;

namespace Player
{
    public class LandingState : State
    {
        float _timePassed = 0f;
        float _landingTime = 1f;
        

        public LandingState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            _timePassed = 0;
            Character.Animator.SetTrigger(Land);
            _landingTime = 0.1f;
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if(_timePassed >= _landingTime)
            {
                Character.Animator.SetTrigger(Move);
                StateMachine.ChangeState(Character.StandingState);
            }
            _timePassed += Time.deltaTime;
        }
    }
}