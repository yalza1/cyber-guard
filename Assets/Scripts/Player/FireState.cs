﻿using UnityEngine;


namespace Player
{
    public class FireState : State
    {
        private bool _grounded;
        private float _gravity;
        private float _playerSpeed;
        private float _targetWeight = 0f;
        private bool _aim;

        private float _timer;
        
        public FireState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }
        
        public override void Enter()
        {
            base.Enter();
            _grounded = false;
            _gravity = Character.Gravity;
            _playerSpeed = Character.playerSpeed / 2;
            _targetWeight = 0f;
            _timer = 0f;
            _aim = false;
            Character.Animator.SetTrigger(Fire);
        }

        public override void HandleInput()
        {
            base.HandleInput();
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            var transform = Camera.main.transform;
            Velocity = Velocity.x * transform.right.normalized + Velocity.z * transform.forward.normalized;
            Velocity.y = 0;

            if (UnityEngine.Input.GetMouseButtonUp(0) || !Character.Gun.CanFire())
            {
                _aim = true;
            }
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.weaponType == WeaponType.Gun && StateMachine.PreviousState == Character.StandingState)
            {
                _targetWeight = Mathf.MoveTowards(_targetWeight,1,Time.deltaTime * 3);
                Character.Animator.SetLayerWeight(1,_targetWeight);
            }
            
            Character.Animator.SetFloat(Speed, Input.magnitude, Character.speedDampTime, Time.deltaTime);
            Character.Animator.SetFloat("xAxis", Input.x, Character.speedDampTime, Time.deltaTime);

            if (_aim)
            {
                if(StateMachine.PreviousState == Character.StandingState)
                {
                    StateMachine.ChangeState(Character.StandingState);
                }
                else
                {
                    StateMachine.ChangeState(Character.AimState);
                }
            }
            
            if(_timer > Character.Gun.FireRate)
            {
                Character.Gun.Fire();
                _timer = 0f;
            }
            else
            {
                _timer += Time.deltaTime;
            }

        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;

            if (_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = -0f;
            }

            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }
        
        public override void Exit()
        {
            base.Exit();
        }
    }
}