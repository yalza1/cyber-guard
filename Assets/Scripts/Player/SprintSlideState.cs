﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UIElements;

namespace Player
{
    public class SprintSlideState : State
    {
        private float _timePassed = 0f;
        private float _slideTime;
        private float _gravity;
        private bool _grounded;
        private float _playerSpeed;
        
        public SprintSlideState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            Character.AudioSource.PlayOneShot(Character.sprintSlideSound);
            Character.AudioSource.pitch = 3;
            Character.AudioSource.volume = 1f;
            
            Character.cameraPivot.GetComponent<Animator>().SetTrigger("slide");

            _timePassed = 0f;
            _gravity = Character.Gravity;
            _slideTime = 1f;
            _playerSpeed = Character.sprintSlideSpeed;
            GravityVelocity.y = 0f;
            _grounded = Character.Grounded;
            Character.Animator.SetTrigger(SprintSlide);
        }

        public override void HandleInput()
        {
            base.HandleInput();
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            
            Velocity = Velocity.x * Character.CameraTransform.right.normalized + Velocity.z * Character.CameraTransform.forward.normalized;
            Velocity.y = 0;
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (_timePassed >= _slideTime)
            {
                StateMachine.ChangeState(Character.SprintState);
            }
            _timePassed += Time.deltaTime;
        }
        
        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
            if(_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = 0f;
            }
            
            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }
        
        public override void Exit()
        {
            base.Exit();
            Character.Animator.ResetTrigger(SprintSlide);
            Character.AudioSource.Stop();
        }
    }
}