﻿using Core;
using UnityEngine;

namespace Player
{
    public class DyingState : State
    {
        private bool _grounded;
        private float _gravity;

        private float _timer;
        
        
        public DyingState(Character character,StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }
        
        public override void Enter()
        {
            base.Enter();
            Character.Animator.SetTrigger(Die);
            _grounded = false;
            _gravity = Character.Gravity;
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.isDead) return;
            if (_timer >= 2f)
            {
                Character.isDead = true;
                Observer.Instant.NotifyObservers(Constant.GameOver);
            }
            _timer += Time.deltaTime;
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;
            
            if (_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = -0f;
            }

        }
    }
}