﻿using System;
using Core;
using UnityEngine;
using UnityEngine.Serialization;
using Weapon;

namespace Player
{
    public class EquipmentSystem : MonoBehaviour
    {
        Character _character;
        
        private GameObject _weaponHolder;
        private GameObject _weaponSheath;
        
        [SerializeField] private GameObject swordHolder;
        [SerializeField] private GameObject swordSheath;
        
        [SerializeField] private GameObject gunHolder;
        [SerializeField] private GameObject gunSheath;

        private void Awake()
        {
            _character = GetComponentInParent<Character>();
        }

        private void Start()
        {
           
        }

        public void DrawWeapon()
        {
            Check();
            
            _weaponHolder.SetActive(true);
            _weaponSheath.SetActive(false);
        }
        
        public void SheathWeapon()
        {
            Check();
            
            _weaponHolder.SetActive(false);
            _weaponSheath.SetActive(true);
        }

        private void Check()
        {
            Observer.Instant.NotifyObservers(Constant.UpdateIconWeapons,(int)_character.weaponType);
            if(_character.weaponType == WeaponType.Sword)
            {
                _weaponHolder = swordHolder;
                _weaponSheath = swordSheath;
            }
            else if(_character.weaponType == WeaponType.Gun)
            {
                _weaponHolder = gunHolder;
                _weaponSheath = gunSheath;
            }
        }
    }
}