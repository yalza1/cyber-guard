﻿using UnityEngine;

namespace Player
{
    public class MeleeAttack : State
    {
        protected float Gravity;
        protected float PlayerSpeed;
        public MeleeAttack(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }
        
        public override void Enter()
        {
            base.Enter();
            Character.Animator.SetTrigger(MeleeAttack);
            Gravity = Character.Gravity;
            PlayerSpeed = 0f;
            GravityVelocity.y = 0f;
        }
        
        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.9 && !Character.Animator.IsInTransition(0))
            {
                StateMachine.ChangeState(Character.StandingState);
            }
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += Gravity * Time.deltaTime;
            Character.Rigidbody.velocity = Velocity * PlayerSpeed * Time.deltaTime + GravityVelocity;
        }
    }
}