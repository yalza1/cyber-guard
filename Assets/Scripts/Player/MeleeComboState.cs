﻿namespace Player
{
    public class MeleeComboState : MeleeAttack
    {
        
        public MeleeComboState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }
        
        public override void Enter()
        {
            Character.Animator.SetTrigger(MeleeAttackCombo);
            Gravity = Character.Gravity;
            PlayerSpeed = 0f;
            GravityVelocity.y = 0f;
        }
    }
}