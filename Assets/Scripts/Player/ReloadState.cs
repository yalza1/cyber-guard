﻿using UnityEngine;

namespace Player
{
    public class ReloadState : State
    {
        private bool _grounded;
        private float _gravity;
        private float _playerSpeed;
        private float _targetWeight = 0f;
        private float _timer;
        private float _reloadTime = 2f;
        
        public ReloadState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            this.Character = character;
            this.StateMachine = stateMachine;
        }

        public override void Enter()
        {
            base.Enter();
            _timer = 0f;
            _reloadTime = 2.5f;
            _grounded = false;
            _gravity = Character.Gravity;
            _playerSpeed = Character.playerSpeed / 2;
            _targetWeight = 0f;
            Character.Animator.SetTrigger(Reload);
            Character.Gun.Reload();
        }


        public override void HandleInput()
        {
            base.HandleInput();
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            var transform = Camera.main.transform;
            Velocity = Velocity.x * transform.right.normalized + Velocity.z * transform.forward.normalized;
            Velocity.y = 0;
            
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.weaponType == WeaponType.Gun)
            {
                _targetWeight = Mathf.MoveTowards(_targetWeight,1,Time.deltaTime * 3);
                Character.Animator.SetLayerWeight(1,_targetWeight);
            }
            
            Character.Animator.SetFloat(Speed, Input.magnitude, Character.speedDampTime, Time.deltaTime);
            Character.Animator.SetFloat("xAxis", Input.x, Character.speedDampTime, Time.deltaTime);

            if (_timer >= _reloadTime)
            {
                Character.Animator.SetTrigger(Move);
                StateMachine.ChangeState(Character.StandingState);
            }
            _timer += Time.deltaTime;
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;

            if (_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = -0f;
            }

            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
   
}