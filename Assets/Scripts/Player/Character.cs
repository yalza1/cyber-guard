﻿using System;
using Core;
using UnityEngine;
using UnityEngine.InputSystem;
using Weapon;


namespace Player
{
    public enum WeaponType
    {
        None,
        Sword,
        Gun,
    }

    public class Character : Singleton<Character>
    {
        public Transform cameraPivot;
        public WeaponType weaponType;
        public bool isDead;

        [Header("Controls")] public float playerSpeed = 5f;
        public float crouchSpeed = 2f;
        public float sprintSpeed = 10f;
        public float sprintSlideSpeed = 10f;
        public float jumpHeight = 1f;
        public float gravityMultiplier = 2f;

        [Header("Animation smoothing")] [Range(0, 1)]
        public float speedDampTime = 0.1f;
        
        [Header("Audio")] public AudioClip walkSound;
        public AudioClip jumpSound;
        public AudioClip sprintSlideSound;

        private Animator _animator;
        private PlayerInput _playerInput;
        private Rigidbody _rigidbody;
        private Transform _cameraTransform;
        private AudioSource _audioSource;
        private EquipmentSystem _equipmentSystem;
        private Gun _gun;

        private float _gravity = -9.81f;

        public StateMachine MovementStateMachine;
        public StandingState StandingState;
        public CrouchingState CrouchingState;
        public JumpingState JumpingState;
        public SprintState SprintState;
        public SprintJumpState SprintJumpState;
        public LandingState LandingState;
        public SprintSlideState SprintSlideState;
        public MeleeAttack MeleeAttack;
        public MeleeComboState MeleeComboState;
        public AimState AimState;
        public FireState FireState;
        public ReloadState ReloadState;
        public DyingState DyingState;
        

        public PlayerInput PlayerInput => _playerInput;
        public Rigidbody Rigidbody => _rigidbody;
        public float Gravity => _gravity;
        public Transform CameraTransform => _cameraTransform;
        public Vector3 PlayerVelocity { get; set; }

        public Animator Animator => _animator;
        public AudioSource AudioSource => _audioSource;
        public float JumpHeight => jumpHeight;

        private CheckIsGrounded _checkIsGrounded;
        private static readonly int WeaponType1 = Animator.StringToHash("weaponType");
        public bool Grounded => _checkIsGrounded.Check();
        
        public EquipmentSystem EquipmentSystem => _equipmentSystem;
        public Gun Gun => _gun;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _gun = GetComponentInChildren<Gun>(true);
            _animator = GetComponentInChildren<Animator>();
            _playerInput = GetComponent<PlayerInput>();
            _rigidbody = GetComponent<Rigidbody>();
            _equipmentSystem = GetComponentInChildren<EquipmentSystem>();
            _checkIsGrounded = GetComponentInChildren<CheckIsGrounded>();
            if (Camera.main != null) _cameraTransform = Camera.main.transform;
        }

        private void Start()
        {
            weaponType = WeaponType.None;
            _gravity *= gravityMultiplier;

            MovementStateMachine = new StateMachine();
            StandingState = new StandingState(this, MovementStateMachine);
            CrouchingState = new CrouchingState(this, MovementStateMachine);
            JumpingState = new JumpingState(this, MovementStateMachine);
            SprintState = new SprintState(this, MovementStateMachine);
            SprintJumpState = new SprintJumpState(this, MovementStateMachine);
            LandingState = new LandingState(this, MovementStateMachine);
            SprintSlideState = new SprintSlideState(this, MovementStateMachine);
            MeleeAttack = new MeleeAttack(this, MovementStateMachine);
            MeleeComboState = new MeleeComboState(this, MovementStateMachine);
            AimState = new AimState(this, MovementStateMachine);
            FireState = new FireState(this, MovementStateMachine);
            ReloadState = new ReloadState(this, MovementStateMachine);
            DyingState = new DyingState(this, MovementStateMachine);

            MovementStateMachine.Initialize(StandingState);
        }

        private void Update()
        {
            MovementStateMachine.CurrentState.HandleInput();
            MovementStateMachine.CurrentState.LogicUpdate();
            _animator.SetFloat(WeaponType1, (float) weaponType);
            
        }

        private void FixedUpdate()
        {
            MovementStateMachine.CurrentState.PhysicsUpdate();
        }
    }
}