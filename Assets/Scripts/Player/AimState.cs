﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class AimState : State
    {
        private bool _grounded;
        private float _gravity;
        private float _playerSpeed;
        private float _targetWeight = 0f;

        private bool _fire;
        private bool _reload;
        
        public AimState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
            Character = character;
            StateMachine = stateMachine;
        }
        
        public override void Enter()
        {
            base.Enter();
            _grounded = false;
            _gravity = Character.Gravity;
            _playerSpeed = Character.playerSpeed / 2;
            _targetWeight = 0f;
            _fire = false;
            _reload = false;
            Character.Animator.SetTrigger(Aim);
        }

        public override void HandleInput()
        {
            base.HandleInput();
            Input = MoveAction.ReadValue<Vector2>();
            Velocity = new Vector3(Input.x, 0f, Input.y);
            var transform = Camera.main.transform;
            Velocity = Velocity.x * transform.right.normalized + Velocity.z * transform.forward.normalized;
            Velocity.y = 0;
            
            if (UnityEngine.Input.GetMouseButton(0))
            {
                if (Character.Gun.CanFire())
                {
                    _fire = true;
                }
                else if(Character.Gun.CanReload())
                {
                    _reload = true;
                }
            }

            if (ReloadAction.triggered && Character.Gun.CanReload())
            {
                _reload = true;
            }
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Character.weaponType == WeaponType.Gun && StateMachine.PreviousState == Character.StandingState)
            {
                _targetWeight = Mathf.MoveTowards(_targetWeight,1,Time.deltaTime * 3);
                Character.Animator.SetLayerWeight(1,_targetWeight);
            }
            
            Character.Animator.SetFloat(Speed, Input.magnitude, Character.speedDampTime, Time.deltaTime);
            Character.Animator.SetFloat("xAxis", Input.x, Character.speedDampTime, Time.deltaTime);
            
            if(AttackSpecialAction.triggered)
            {
                StateMachine.ChangeState(Character.StandingState);
            }
            if (_fire)
            {
                StateMachine.ChangeState(Character.FireState);
            }
            if (_reload)
            {
                StateMachine.ChangeState(Character.ReloadState);
            }
        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
            GravityVelocity.y += _gravity * Time.deltaTime;
            _grounded = Character.Grounded;

            if (_grounded && GravityVelocity.y < 0)
            {
                GravityVelocity.y = -0f;
            }

            Character.Rigidbody.velocity = Velocity * _playerSpeed * Time.deltaTime + GravityVelocity;
        }
        
        public override void Exit()
        {
            base.Exit();
        }
    }
}