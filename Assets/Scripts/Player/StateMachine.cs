﻿using UnityEngine;

namespace Player
{
    public class StateMachine
    {
        public State CurrentState;
        public State PreviousState;
        
        public void Initialize(State startingState)
        {
            CurrentState = startingState;
            CurrentState.Enter();
        }
        
        public void ChangeState(State newState)
        {
            PreviousState = CurrentState;
            CurrentState.Exit();
            CurrentState = newState;
            newState.Enter();
        }
    }
}