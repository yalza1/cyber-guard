using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Login : MonoBehaviour
    {
        [SerializeField] private InputField username;
        [SerializeField] private InputField password;
        [SerializeField] TextMeshProUGUI errorText;
        
        [SerializeField] GameObject loginPanel;
        [SerializeField] GameObject loadingPanel;
        
        public void OnclickLogin()
        {
            if (username.text == "")
            {
                errorText.text = "Username is empty!";
                errorText.gameObject.SetActive(true);
            }
            else if (password.text == "")
            {
                errorText.text = "Password is empty!";
                errorText.gameObject.SetActive(true);
            }
            else
            {
                errorText.text = "Login success";
                errorText.gameObject.SetActive(true);
                loginPanel.SetActive(false);
                loadingPanel.SetActive(true);
                errorText.color = Color.green;
                
            }
        }
    }
}
