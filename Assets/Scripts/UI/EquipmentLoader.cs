﻿using System;
using System.Globalization;
using System.Net.Mime;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Weapon;

namespace UI
{
    public class EquipmentLoader : MonoBehaviour
    {
        [SerializeField] private GameObject currentWeapon;
        [SerializeField] private TextMeshProUGUI currentWeaponName;
        [SerializeField] private TextMeshProUGUI currentWeaponNameDetail;
        
        [SerializeField] private SwordData[] swordDatas;
        [SerializeField] private Transform swordContent;
        [SerializeField] private GameObject swordPrefab;
        
        [SerializeField] private GunData[] gunDatas;
        [SerializeField] private Transform gunContent;
        [SerializeField] private GameObject gunPrefab;

        [SerializeField] private GameObject equipmentModel;
        
        [Header("Gun Parameters")]
        public TextMeshProUGUI damage;
        public TextMeshProUGUI magazineSize;
        public TextMeshProUGUI reloadTime;
        public TextMeshProUGUI fireRate;
        public TextMeshProUGUI recoil;
        public TextMeshProUGUI falloffRange;
        
        [Header("Sword Parameters")]
        public TextMeshProUGUI damageSword;
        public TextMeshProUGUI durability;
        public TextMeshProUGUI attackSpeed;
        public TextMeshProUGUI weight;
        public TextMeshProUGUI length;
        public TextMeshProUGUI range;
        
        

        private void Awake()
        {
            swordDatas = Resources.LoadAll<SwordData>("Swords Data");
            gunDatas = Resources.LoadAll<GunData>("Guns Data");
        }

        private void Start()
        {
            LoadSwordData();
            LoadGunData();
        }
        
        private void LoadSwordData()
        {
            Array.Sort(swordDatas,(x,y)=> x.rank.CompareTo(y.rank));
            foreach (var swordData in swordDatas)
            {
                var sword = Instantiate(swordPrefab, swordContent);
                SwordItem swordItem = sword.GetComponent<SwordItem>();
                swordItem.swordData = swordData;
                swordItem.parent = equipmentModel;
                
                Image swordImage = sword.transform.GetChild(0).GetComponent<Image>();
                swordImage.sprite = swordData.icon;
                TextMeshProUGUI swordName = sword.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
                swordName.text = swordData.name;
                
                Button swordButton = sword.GetComponent<Button>();
                swordButton.onClick.AddListener(() =>
                {
                    currentWeapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = swordData.name;
                    currentWeapon.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Rank " + swordData.rank;
                    currentWeapon.transform.GetChild(2).GetComponent<Slider>().value = swordData.rank / 10f;
                    currentWeapon.transform.GetChild(3).GetComponent<Image>().sprite = swordData.icon;
                    currentWeaponName.text = swordData.name;
                    currentWeaponNameDetail.text = swordData.description;
                    
                    damageSword.text = swordData.damage.ToString(CultureInfo.InvariantCulture);
                    durability.text = swordData.durability.ToString(CultureInfo.InvariantCulture);
                    attackSpeed.text = swordData.attackSpeed.ToString(CultureInfo.InvariantCulture);
                    weight.text = swordData.weight.ToString(CultureInfo.InvariantCulture);
                    length.text = swordData.length.ToString(CultureInfo.InvariantCulture);
                    range.text = swordData.range.ToString(CultureInfo.InvariantCulture);
                    
                    PlayerPrefs.SetInt(Constant.IDCurrentSword,swordData.id);
                    PlayerPrefs.Save();
                });
            }
            
        }
        
        private void LoadGunData()
        {
            Array.Sort(gunDatas,(x,y)=> x.rank.CompareTo(y.rank));
            foreach (var gunData in gunDatas)
            {
                var gun = Instantiate(gunPrefab, gunContent);
                GunItem gunItem = gun.GetComponent<GunItem>();
                gunItem.gunData = gunData;
                gunItem.parent = equipmentModel;
                
                Image gunImage = gun.transform.GetChild(0).GetComponent<Image>();
                gunImage.sprite = gunData.icon;
                TextMeshProUGUI gunName = gun.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
                gunName.text = gunData.name;
                
                Button gunButton = gun.GetComponent<Button>();
                gunButton.onClick.AddListener(() =>
                {
                    currentWeapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = gunData.name;
                    currentWeapon.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Rank " + gunData.rank;
                    currentWeapon.transform.GetChild(2).GetComponent<Slider>().value = gunData.rank / 10f;
                    currentWeapon.transform.GetChild(3).GetComponent<Image>().sprite = gunData.icon;
                    currentWeaponName.text = gunData.name;
                    currentWeaponNameDetail.text = gunData.description;
                    
                    damage.text = gunData.damage.ToString(CultureInfo.InvariantCulture);
                    magazineSize.text = gunData.magazineSize.ToString();
                    reloadTime.text = gunData.reloadTime.ToString(CultureInfo.InvariantCulture);
                    fireRate.text = gunData.fireRate.ToString(CultureInfo.InvariantCulture);
                    recoil.text = gunData.recoil.ToString(CultureInfo.InvariantCulture);
                    falloffRange.text = gunData.falloffRange.ToString(CultureInfo.InvariantCulture);
                    
                    PlayerPrefs.SetInt(Constant.IDCurrentGun,gunData.id);
                    PlayerPrefs.Save();
                });
            }
        }

        public void EquipSwordButton()
        {
            int id = PlayerPrefs.GetInt(Constant.IDCurrentSword);

            foreach (var swordData in swordDatas)
            {
                if (id == swordData.id)
                {
                    currentWeapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = swordData.name;
                    currentWeapon.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Rank " + swordData.rank;
                    currentWeapon.transform.GetChild(2).GetComponent<Slider>().value = swordData.rank / 10f;
                    currentWeapon.transform.GetChild(3).GetComponent<Image>().sprite = swordData.icon;
                    currentWeaponName.text = swordData.name;
                    currentWeaponNameDetail.text = swordData.description;
                    
                    damageSword.text = swordData.damage.ToString(CultureInfo.InvariantCulture);
                    durability.text = swordData.durability.ToString(CultureInfo.InvariantCulture);
                    attackSpeed.text = swordData.attackSpeed.ToString(CultureInfo.InvariantCulture);
                    weight.text = swordData.weight.ToString(CultureInfo.InvariantCulture);
                    length.text = swordData.length.ToString(CultureInfo.InvariantCulture);
                    range.text = swordData.range.ToString(CultureInfo.InvariantCulture);
                    
                    GameObject model = Instantiate(swordData.prefab, equipmentModel.transform);
                    Image image = model.GetComponent<Image>();
                    image.color = new Color(0, 0, 0, 0);
                    image.raycastTarget = false;
                    GameObject wp = equipmentModel.transform.GetChild(0).gameObject;
                    if(wp)Destroy(wp);
            
                    model.transform.localPosition = Vector3.zero;
                    model.transform.localRotation = Quaternion.identity;
                    model.transform.localScale = Vector3.one * 2;
                }
            }
        }
        
        public void EquipGunButton()
        {
            int id = PlayerPrefs.GetInt(Constant.IDCurrentGun);

            foreach (var gunData in gunDatas)
            {
                if (id == gunData.id)
                {
                    currentWeapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = gunData.name;
                    currentWeapon.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Rank " + gunData.rank;
                    currentWeapon.transform.GetChild(2).GetComponent<Slider>().value = gunData.rank / 10f;
                    currentWeapon.transform.GetChild(3).GetComponent<Image>().sprite = gunData.icon;
                    currentWeaponName.text = gunData.name;
                    currentWeaponNameDetail.text = gunData.description;
                    
                    damage.text = gunData.damage.ToString(CultureInfo.InvariantCulture);
                    magazineSize.text = gunData.magazineSize.ToString();
                    reloadTime.text = gunData.reloadTime.ToString(CultureInfo.InvariantCulture);
                    fireRate.text = gunData.fireRate.ToString(CultureInfo.InvariantCulture);
                    recoil.text = gunData.recoil.ToString(CultureInfo.InvariantCulture);
                    falloffRange.text = gunData.falloffRange.ToString(CultureInfo.InvariantCulture);
                    
                    GameObject model = Instantiate(gunData.prefab,equipmentModel.transform);
                    Image image = model.GetComponent<Image>();
                    image.color = new Color(0, 0, 0, 0);
                    image.raycastTarget = false;
                    GameObject wp = equipmentModel.transform.GetChild(0).gameObject;
                    if(wp)Destroy(wp);
            
                    model.transform.localPosition = Vector3.zero;
                    model.transform.localRotation = Quaternion.identity;
                    model.transform.localScale = Vector3.one * 2;
                }
            }
        }
    }
}