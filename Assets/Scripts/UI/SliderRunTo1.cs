﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
	public class SliderRunTo1 : MonoBehaviour
	{

		public String sceneName;
		public bool b = true;
		public Slider slider;
		public float speed = 0.5f;

		float _time = 0f;

		void Start()
		{

			slider = GetComponent<Slider>();
		}

		void Update()
		{
			if (b)
			{
				_time += Time.deltaTime * speed;
				slider.value = _time;

				if (_time > 1)
				{
					SceneManager.LoadScene(sceneName);
					b = false;
					_time = 0;
				}
			}
		}


	}
}
