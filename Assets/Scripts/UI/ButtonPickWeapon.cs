﻿using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ButtonPickWeapon : MonoBehaviour
    {
        [SerializeField] private GameObject panelWeaponModel;
        
        
        public void OnClick()
        {
            GameObject model = Instantiate(gameObject.transform.GetChild(0).gameObject, panelWeaponModel.transform, true);
            Image image = model.GetComponent<Image>();
            image.color = new Color(0, 0, 0, 0);
            image.raycastTarget = false;

            GameObject wp = panelWeaponModel.transform.GetChild(0).gameObject;
            if(wp)Destroy(wp);

            model.transform.localPosition = Vector3.zero;
            model.transform.localRotation = Quaternion.identity;
            model.transform.localScale = Vector3.one * 2;

            TextMeshProUGUI nameWeapon = gameObject.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
            PlayerPrefs.SetString(Constant.CurrentWeaponName,nameWeapon.text);
            PlayerPrefs.Save();
            Observer.Instant.NotifyObservers(Constant.ChangeWeapon);
        }
    }
}