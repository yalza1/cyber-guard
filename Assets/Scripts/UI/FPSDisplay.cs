﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace UI
{
    public class FPSDisplay : MonoBehaviour
    {
        public TextMeshProUGUI fpsText;
        private float _fps;

        private void Start()
        {
            InvokeRepeating(nameof(GetFps), 1f, 1f);
            Application.targetFrameRate = 300;
            QualitySettings.vSyncCount = (int)Screen.currentResolution.refreshRateRatio.value / Application.targetFrameRate;
        }

        void GetFps()
        {
            _fps = 1.0f / Time.unscaledDeltaTime;
            fpsText.text = $"FPS: {(_fps)}";
        }
        
    }
}