﻿using Core;
using UnityEngine;

namespace UI
{
    public class EquipClick : MonoBehaviour
    {
        [SerializeField] private int _index;
        
        public void OnClick()
        {
            PlayerPrefs.SetInt(Constant.EquipIndex,_index);
            PlayerPrefs.Save();
            Observer.Instant.NotifyObservers(Constant.EquipClick);
        }
    }
}