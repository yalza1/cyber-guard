﻿using System;
using Core;
using TMPro;
using UnityEngine;

namespace UI
{
    public class SetCurrentWeapon : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI name;
        [SerializeField] private TextMeshProUGUI nameDetail;
        

        private void Start()
        {
            Observer.Instant.RegisterObserver(Constant.ChangeWeapon,SetInformation);
        }


        private void SetInformation()
        {
            name.text = PlayerPrefs.GetString(Constant.CurrentWeaponName);
            nameDetail.text = PlayerPrefs.GetString(Constant.CurrentWeaponName);
        }
    }
}