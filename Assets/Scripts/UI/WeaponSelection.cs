﻿using System;
using Core;
using UnityEngine;

namespace UI
{
    public class WeaponSelection : MonoBehaviour
    {
        [SerializeField] private GameObject[] _weapons;
        [SerializeField] private GameObject _weaponSelection;
        [SerializeField] private GameObject _equipPanel;

        private void Start()
        {
            Observer.Instant.RegisterObserver(Constant.EquipClick,ActiveScrollView);
        }

        private void ActiveScrollView()
        {
            _weaponSelection.SetActive(true);
            _equipPanel.SetActive(false);
            
            int index = PlayerPrefs.GetInt(Constant.EquipIndex);
            for(int i = 0; i< _weapons.Length; i++)
            {
                _weapons[i].SetActive(i == index);
            }
        }
        
        
    }
}