﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Image = UnityEngine.UI.Image;

namespace Inventory
{
    public class InventoryUI : MonoBehaviour
    {
        public Inventory inventory;
        
        public List<Transform> itemSlots;
        
        public Vector3 originalPosition;
        public Vector3 hidePosition;
        public bool isShow = false;

        private void Start()
        {
            inventory.ItemAdded += InventoryOnItemAdded;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (isShow)
                {
                    isShow = false;
                    transform.DOLocalMove(hidePosition, 0.5f);
                }
                else
                {
                    isShow = true;
                    transform.DOLocalMove(originalPosition, 0.5f);
                }
            }
        }


        private void InventoryOnItemAdded(object sender, InventoryEventArgs e)
        {
            foreach (var slot in itemSlots)
            {
                Image image = slot.GetChild(0).GetComponent<Image>();
                if (!image.enabled)
                {
                    image.enabled = true;
                    image.sprite = e.Item.Icon;
                    break;
                }
            }
        }
    }
}