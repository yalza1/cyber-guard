﻿using System;
using Interfaces;

namespace Inventory
{
    public class InventoryEventArgs : EventArgs
    { 
        public InventoryEventArgs(IInventoryItem item)
        {
            Item = item;
        }
        public readonly IInventoryItem Item;
    }
}