﻿using System;
using System.Collections.Generic;
using Interfaces;
using UnityEngine;

namespace Inventory
{
    public class Inventory : MonoBehaviour
    {
        private const int Slots = 8;
        private readonly List<IInventoryItem> _items = new List<IInventoryItem>();

        public event EventHandler<InventoryEventArgs> ItemAdded;
        
        private Transform _cameraTransform;

        public GameObject interactText;
        private void Start()
        {
            _cameraTransform = Camera.main.transform;
        }

        private void AddItem(IInventoryItem item)
        {
            if (_items.Count < Slots)
            {


                _items.Add(item);
                item.Onpickup();
                if (ItemAdded != null)
                {
                    ItemAdded(this, new InventoryEventArgs(item));
                }
            }

        }

        private void Update()
        {
            RaycastHit hit;
            Ray ray = new Ray(_cameraTransform.position, _cameraTransform.forward);
            if (Physics.Raycast(ray, out hit, 10f))
            {
                var hitObject = hit.collider.gameObject;
                var item = hitObject.GetComponentInParent<IInventoryItem>();
                if (item != null)
                {
                    interactText.SetActive(true);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        PickupItem(item);
                    }
                }
                else
                {
                    interactText.SetActive(false);
                }
            }
        }
        
        private void PickupItem(IInventoryItem item)
        {
            AddItem(item);
        }
    }
}