﻿using System;
using Core;
using Tayx.Graphy.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Misson
{
    public class MissonManager : G_Singleton<MissonManager>
    {
        
        public MissonInformation currentMisson;
        [SerializeField] private Transform missonParentContent;
        [SerializeField] private GameObject missonPrefab;

        [SerializeField] private GameObject missonMenu;
        [SerializeField] private GameObject missonPanel;
        
        [SerializeField] private MissonInformation[] missonInformations;
        
        private void Awake()
        {
            
            missonInformations = Resources.LoadAll<MissonInformation>("MissonInformations");
            
        }
        private void Start()
        {
            DontDestroyOnLoad(transform.root.gameObject);
            LoadMisson();
            
        }

        private void LoadMisson()
        {
            Array.Sort(missonInformations,(x,y)=> x.isLocked.CompareTo(y.isLocked));
            
            foreach (var missonData in missonInformations)
            {
                var misson = Instantiate(missonPrefab, missonParentContent);
                var missonComponent = misson.GetComponent<Misson>();
                missonComponent.missonInformation = missonData;
                var missonButton = misson.GetComponent<Button>();
                missonComponent.missonName.text = missonData.missonName;
                missonComponent.background.sprite = missonData.image;
                if (missonData.isLocked)
                {
                    missonComponent.lockedPanel.SetActive(true);
                    missonComponent.unlockedPanel.SetActive(false);
                    missonButton.interactable = false;
                }
                else
                {
                    missonComponent.lockedPanel.SetActive(false);
                    missonComponent.unlockedPanel.SetActive(true);
                    missonButton.interactable = true;

                    missonComponent.unlockedPanel.transform.GetChild(0).GetChild(0).gameObject
                        .SetActive(missonData.isCompleteQuest1);
                    missonComponent.unlockedPanel.transform.GetChild(1).GetChild(0).gameObject
                        .SetActive(missonData.isCompleteQuest2);
                    missonComponent.unlockedPanel.transform.GetChild(2).GetChild(0).gameObject
                        .SetActive(missonData.isCompleteQuest3);
                }
                
                missonButton.onClick.AddListener(() => SelectMisson(missonData));
            }
        }

        private void SelectMisson(MissonInformation missonInformation)
        {
            currentMisson = missonInformation;
            missonMenu.SetActive(false);
            missonPanel.SetActive(true);
            LoadMissonInfor();
        }

        public void BeginMisson()
        {
            SceneManager.LoadScene(currentMisson.missonName);
        }

        public void ExitMisson()
        {
            missonPanel.SetActive(false);
            missonMenu.SetActive(true);
        }

        private void LoadMissonInfor()
        {
            var beginMissonPanel = missonPanel.GetComponent<BeginMissonPanel>();
            beginMissonPanel.background.sprite = currentMisson.image;
            beginMissonPanel.missonName.text = currentMisson.missonName;
            beginMissonPanel.missonDescription.text = currentMisson.missonDescription;

            beginMissonPanel.quest1Panel.transform.GetChild(0).GetChild(0).gameObject
                .SetActive(currentMisson.isCompleteQuest1);
            beginMissonPanel.quest2Panel.transform.GetChild(0).GetChild(0).gameObject
                .SetActive(currentMisson.isCompleteQuest2);
            beginMissonPanel.quest3Panel.transform.GetChild(0).GetChild(0).gameObject
                .SetActive(currentMisson.isCompleteQuest3);
            
            beginMissonPanel.quest1Panel.transform.GetComponentInChildren<TextMeshProUGUI>().text =
                currentMisson.quest1Description;
            beginMissonPanel.quest2Panel.transform.GetComponentInChildren<TextMeshProUGUI>().text =
                currentMisson.quest2Description;
            beginMissonPanel.quest3Panel.transform.GetComponentInChildren<TextMeshProUGUI>().text =
                currentMisson.quest3Description;
        }
        
        public void UpdateQuests(object param)
        {
            if (param is not (bool, bool,bool)) return;
               var (isCompleteQuest1, isCompleteQuest2, isCompleteQuest3) = ((bool, bool,bool)) param;
               currentMisson.isCompleteQuest1 = isCompleteQuest1;
               currentMisson.isCompleteQuest2 = isCompleteQuest2;
               currentMisson.isCompleteQuest3 = isCompleteQuest3;
        }
        
    }
}