﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Misson
{
    public class BeginMissonPanel : MonoBehaviour
    {
        [SerializeField] public Image background;
        [SerializeField] public TextMeshProUGUI missonName;
        [SerializeField] public TextMeshProUGUI missonDescription;
        
        [SerializeField] public GameObject quest1Panel;
        [SerializeField] public GameObject quest2Panel;
        [SerializeField] public GameObject quest3Panel;
        
    }
}