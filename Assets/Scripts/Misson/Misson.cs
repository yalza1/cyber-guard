﻿
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Misson
{
    public class Misson : MonoBehaviour
    {
        public MissonInformation missonInformation;
        [SerializeField] public Image background;
        [SerializeField] public TextMeshProUGUI missonName;

        [SerializeField] public GameObject lockedPanel;
        [SerializeField] public GameObject unlockedPanel;
    }
}