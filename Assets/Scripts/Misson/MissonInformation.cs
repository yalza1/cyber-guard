﻿using UnityEngine;

namespace Misson
{
    [CreateAssetMenu(fileName = "MissonInformation", menuName = "Misson/MissonInformation")]
    public class MissonInformation : ScriptableObject
    {
        public string missonId;
        public Sprite image;
        public string missonName;
        public bool isLocked;
        [Space][TextArea(3, 10)]
        public string missonDescription;
        
        
        [Space][TextArea(3, 10)]
        public string quest1Description;
        public bool isCompleteQuest1;
        [Space][TextArea(3, 10)]
        public string quest2Description;
        public bool isCompleteQuest2;
        [Space][TextArea(3, 10)]
        public string quest3Description;
        public bool isCompleteQuest3;
        [Space]
        public bool isCompleteMisson;
    }
}