﻿using Core;
using UnityEngine;

namespace Items
{
    public class ItemManager : Singleton<ItemManager>
    {
        public GameObject[] items;

        public void DropRandomItem(Vector3 position, Quaternion rotation)
        {
            int randomItem = Random.Range(0, items.Length);
            Instantiate(items[randomItem], position, rotation);
        }
    }
}