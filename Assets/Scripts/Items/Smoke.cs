﻿using Interfaces;
using UnityEngine;

namespace Items
{
    public class Smoke : MonoBehaviour , IInventoryItem
    {
        public Sprite icon;
        public new string name;
        public string Name => name;
        public Sprite Icon => icon;
        public void Onpickup()
        {
            gameObject.SetActive(false);
        }
    }
}