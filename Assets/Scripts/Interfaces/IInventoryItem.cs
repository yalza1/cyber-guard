﻿using UnityEngine;

namespace Interfaces
{
    public interface IInventoryItem
    {
        string Name { get; }
        Sprite Icon { get; }
        void Onpickup();
    }
}