﻿namespace Interfaces
{
    public interface IDropItemWhenDie
    {
        public void DropItem();
    }
}