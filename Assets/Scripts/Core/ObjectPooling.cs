﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class ObjectPooling : Singleton<ObjectPooling>
    {
        readonly Dictionary<GameObject, List<GameObject>> _listObject = new Dictionary<GameObject, List<GameObject>>();
        public GameObject GetGameObject(GameObject obj,Vector3 position,Quaternion rotation)
        {
            if (_listObject.ContainsKey(obj))
            {
                foreach (GameObject go in _listObject[obj])
                {
                    if (go.activeSelf) continue;
                    go.transform.position = position;
                    go.transform.rotation = rotation;
                    return go;
                }
                GameObject g = Instantiate(obj, position, rotation);
                _listObject[obj].Add(g);
                g.SetActive(false);

                return g;
            }

            List<GameObject> list = new List<GameObject>();
            GameObject g2 = Instantiate(obj, position, rotation);
            list.Add(g2);
            g2.SetActive(false);
            _listObject.Add(obj, list);

            return g2;

        }
    }
}