﻿using UnityEngine;

namespace Core
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {

        private static T _instant;
        public static T Instant
        {
            get
            {
                if (_instant == null)
                {
                    if (FindObjectOfType<T>() != null)
                        _instant = FindObjectOfType<T>();
                    else
                        new GameObject().AddComponent<T>().name = "Singleton_" + typeof(T);
                }

                return _instant;
            }
        }

        void Awake()
        {
            if (_instant != null && _instant.gameObject.GetInstanceID() != gameObject.GetInstanceID())
            {
                Debug.LogError("Singleton already exist " + _instant.gameObject.name);
                Destroy(gameObject);
            }
            else
                _instant = GetComponent<T>();
        }
    }
}