﻿using Misson;
using UnityEngine;

namespace Core
{
    public class GameManager : Singleton<GameManager>
    {
        public bool isWin;
        private int _timer;
        private MissonManager _missonManager;
        
        public bool bossDead = false;
        
        private void Start()
        {
            _timer = 0;
            InvokeRepeating(nameof(Timer), 0, 1);
            _missonManager = FindObjectOfType<MissonManager>();
        }

        public void GameOver()
        {
            bool q2 = _timer <= 300;
            bool q3 = bossDead;
            _missonManager.UpdateQuests((true,q2,q3));
            Observer.Instant.NotifyObservers(Constant.UpdateResultStars, (true, q2, q3));
            Clear();
            Debug.Log("Game Over");
        }

        private void Clear()
        {
            StopAllCoroutines();
            CancelInvoke(nameof(Timer));
            Destroy(_missonManager);
        }
        
        private void Timer()
        {
            _timer++;
            Observer.Instant.NotifyObservers(Constant.UpdateTime,_timer);
        }
    }
}