﻿
using UnityEngine;

namespace Core
{
    public class DontDestroyOnload : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);  
        }
    }
}