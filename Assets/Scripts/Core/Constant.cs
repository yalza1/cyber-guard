﻿namespace Core
{
    internal static class Constant
    {
        internal static readonly string AcuracyCrosshair = "acuracyCrosshair";
        internal static readonly string UpdateHealthBar = "updateHealthBar";
        internal static readonly string UpdateShieldBar = "updateShieldBar";
        
        internal static readonly string GameOver = "gameOver";
        internal static readonly string EquipIndex = "equipIndex";
        internal static readonly string CurrentWeaponName = "currentWeaponName";
        internal static readonly string ChangeWeapon = "changeWeapon";
        internal static readonly string EquipClick = "equipClick";
        internal static readonly string UpdateAmmo = "updateAmmo";
        internal static readonly string UpdateMaxAmmo = "updateMaxAmmo";
        
        internal static readonly string UpdateIconWeapons = "updateIconWeapon";
        internal static readonly string UpdateTime = "updateTime";
        
        internal static readonly string UpdateQuests = "updateQuests";
        
        internal static readonly string UpdateResultStars = "updateResultStars";

        internal static readonly string IDCurrentSword = "idCurrentSword";
        internal static readonly string IDCurrentGun = "idCurrentGun";
    }
}