﻿
using System.Globalization;
using Misson;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Core
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject optionPanel;
        
        [FormerlySerializedAs("_healthBar")]
        [Header("Health Bar")] 
        [SerializeField] private Slider healthBar;
        [SerializeField] private Slider shieldBar;
        [SerializeField] private Slider ammoBar;
        private TextMeshProUGUI _healthText, _shieldText;
        [SerializeField] private GameObject gameOverpanel;
        [SerializeField] private TextMeshProUGUI resultText;
        [SerializeField] private TextMeshProUGUI maxAmmoText;

        [SerializeField] private GameObject[] iconWeapons;

        [SerializeField] private TextMeshProUGUI time;

        [SerializeField] private GameObject resultStarts;
        

        private void Awake()
        {
            _healthText = healthBar.GetComponentInChildren<TextMeshProUGUI>();
            _shieldText = shieldBar.GetComponentInChildren<TextMeshProUGUI>();
            Observer.Instant.RegisterObserver(Constant.UpdateHealthBar,UpdateHealthBar);
            Observer.Instant.RegisterObserver(Constant.UpdateShieldBar,UpdateShieldBar);
            Observer.Instant.RegisterObserver(Constant.GameOver,UpdateGameOver);
            Observer.Instant.RegisterObserver(Constant.UpdateAmmo,UpdateSliderAmmo);
            Observer.Instant.RegisterObserver(Constant.UpdateMaxAmmo,UpdateMaxAmmoText);
            Observer.Instant.RegisterObserver(Constant.UpdateIconWeapons,UpdateIconWeapons);
            Observer.Instant.RegisterObserver(Constant.UpdateTime,UpdateTime);
            Observer.Instant.RegisterObserver(Constant.UpdateResultStars,UpdateResultStars);
        }
        
        private void UpdateMaxAmmoText(object param)
        {
            
            if (param is int i)
            {
                maxAmmoText.text = i.ToString();
            }
        }

        private void UpdateSliderAmmo(object param)
        {
            if (param is not (int, int)) return;
            var (currentAmmo, clipSize) = ((int, int)) param;
            ammoBar.value = currentAmmo / (float) clipSize;
        }
        
        private void UpdateHealthBar(object param)
        {
            if (param is not (float, float)) return;
            var (health, maxHealth) = ((float, float)) param;
            healthBar.value = health / maxHealth;
            _healthText.text = health.ToString(CultureInfo.InvariantCulture);
        }
        private void UpdateShieldBar(object param)
        {
            if (param is not (float, float)) return;
            var (shield, maxShield) = ((float, float)) param;
            shieldBar.value = shield / maxShield;
            _shieldText.text = shield.ToString(CultureInfo.InvariantCulture);
        }

        private void UpdateGameOver()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            resultText.text = GameManager.Instant.isWin ? "Mission Complete" : "Mission Failed";
            gameOverpanel.SetActive(true);
        }
        
        public void Continue()
        {
            SceneManager.LoadScene("LobbyRoomScene");
        }

        private void Update()
        {
            if (!Input.GetKeyDown(KeyCode.Escape)) return;
            optionPanel.SetActive(!optionPanel.activeSelf);
            Time.timeScale = optionPanel.activeSelf ? 0 : 1;
            Cursor.visible = optionPanel.activeSelf;
            Cursor.lockState = optionPanel.activeSelf ? CursorLockMode.None : CursorLockMode.Locked;
        }

        public void Resume()
        {
            optionPanel.SetActive(false);
            Time.timeScale = 1;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void UpdateIconWeapons(object index)
        {
            if (index is not int i) return;
            for (int j = 0; j < iconWeapons.Length; j++)
            {
                iconWeapons[j].GetComponent<Image>().color = i == j ? new Color(255, 50, 0 ,255) : new Color(0, 0, 0,255);
            }
        }
        
        private void UpdateTime(object time)
        {
            if (time is not int f) return;
            int a = f / 60;
            int b = f % 60;
            if(a<10 && b < 10)
                this.time.text = "0" + a + ":0" + b;
            else if(a<10 && b >= 10)
                this.time.text = "0" + a + ":" + b;
            else if(a>=10 && b < 10)
                this.time.text = a + ":0" + b;
            else
                this.time.text = a + ":" + b;
        }
        
        
        private void UpdateResultStars(object param)
        {
            if (param is not (bool, bool, bool)) return;
            var (isFirst, isSecond, isThird) = ((bool, bool, bool)) param;
            resultStarts.transform.GetChild(0).GetChild(0).gameObject.SetActive(isFirst);
            resultStarts.transform.GetChild(1).GetChild(0).gameObject.SetActive(isSecond);
            resultStarts.transform.GetChild(2).GetChild(0).gameObject.SetActive(isThird);
        }
        
    }
}