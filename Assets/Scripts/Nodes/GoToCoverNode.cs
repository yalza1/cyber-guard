﻿using Behaviour_Trees;
using Enemies;
using UnityEngine;
using UnityEngine.AI;

namespace Nodes
{
    public class GoToCoverNode : Node
    {
        private readonly NavMeshAgent _agent;
        private readonly EnemyAI _enemyAI;
        
        public GoToCoverNode(NavMeshAgent agent, EnemyAI enemyAI)
        {
            _agent = agent;
            _enemyAI = enemyAI;
        }
        
        public override NodeState Evaluate()
        {
            Transform coverSpot = _enemyAI.BestCoverSpot;
            if(coverSpot == null)
            {
                return NodeState.Failure;
            }
            var distance = Vector3.Distance(_agent.transform.position, coverSpot.position);
            if (distance > 0.2f)
            {
                _agent.isStopped = false;
                _agent.SetDestination(coverSpot.position);
                return NodeState.Running;
            }
            else
            {
                _agent.isStopped = true;
                return NodeState.Success;
            }
        }
    }
}