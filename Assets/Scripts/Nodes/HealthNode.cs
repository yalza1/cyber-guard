﻿using Behaviour_Trees;
using Enemies;

namespace Nodes
{
    public class HealthNode : Node
    {
        private readonly EnemyAI _enemyAI;
        private readonly float _healthThreshold;
        
        public HealthNode(EnemyAI enemyAI, float healthThreshold)
        {
            _enemyAI = enemyAI;
            _healthThreshold = healthThreshold;
        }


        public override NodeState Evaluate()
        {
            return _enemyAI.Health <= _healthThreshold ? NodeState.Success : NodeState.Failure;
        }
    }
}