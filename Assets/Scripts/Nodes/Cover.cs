﻿using UnityEngine;

namespace Nodes
{
    public class Cover : MonoBehaviour
    {
        [SerializeField] private Transform[] coverSpots;
        
        public Transform[] CoverSpots => coverSpots;
    }
}