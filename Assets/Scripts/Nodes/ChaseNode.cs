﻿using Behaviour_Trees;
using UnityEngine;
using UnityEngine.AI;

namespace Nodes
{
    public class ChaseNode : Node
    {
        private readonly Transform _target;
        private readonly NavMeshAgent _agent;
        private readonly bool _withAgent;
        private readonly Animator _animator;
        
        public ChaseNode(Transform target, NavMeshAgent agent, bool withAgent)
        {
            _target = target;
            _agent = agent;
            _withAgent = withAgent;
            _animator = agent.GetComponent<Animator>();
        }
        public override NodeState Evaluate()
        {
            var distance = Vector3.Distance(_agent.transform.position, _target.position);
            if (_withAgent)
            {
                if (distance > 0.2)
                {
                    _agent.isStopped = false;
                    _animator.SetBool("Running", true);

                    _agent.SetDestination(_target.position);
                    return NodeState.Running;
                }       

                _animator.SetBool("Running",false);
   
                _agent.isStopped = true;
                return NodeState.Success;

            }

            if (distance > 0.2f)
            {
                var position = _agent.transform.position;
                var position1 = _target.position;
                Vector3 newTarget = new Vector3(position1.x, position.y, position1.z);
                position = Vector3.MoveTowards(position, newTarget,
                    _agent.speed * Time.deltaTime);
                _agent.transform.position = position;
                return NodeState.Running;
            }
            return NodeState.Success;
        }
    }
}