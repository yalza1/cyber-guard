﻿using Behaviour_Trees;
using UnityEngine;

namespace Nodes
{
    public class IsCoveredNode : Node
    {
        private Transform _target;
        private Transform _origin;
        
        public IsCoveredNode(Transform origin, Transform target)
        {
            _target = target;
            _origin = origin;
        }
        public override NodeState Evaluate()
        {
            RaycastHit hit;
            if(Physics.Raycast(_origin.position, _target.position - _origin.position, out hit))
            {
                if (hit.transform != _target)
                {
                    return NodeState.Success;
                }
            }
            return NodeState.Failure;
        }
    }
}