﻿using Behaviour_Trees;
using Core;
using Enemies;
using Interfaces;
using UnityEngine;
using UnityEngine.AI;
using Object = System.Object;

namespace Nodes
{
    public class ShootNode : Node
    {
        private readonly Transform _head;
        private readonly Transform _target;
        private readonly ShootProperties _shootProperties;

        private float _timmer;
        
        public ShootNode(Transform head,Transform target, ShootProperties shootProperties)
        {
            _head = head;
            _target = target;
            _shootProperties = shootProperties;
        }

        public override NodeState Evaluate()
        {
            _head.LookAt(_target.position + _shootProperties.targetOffset);
            if (_timmer > _shootProperties.timeBetweenShots)
            {
                Shoot();
                _timmer = 0f;
            }

            _timmer += Time.deltaTime;
            return NodeState.Running;
        }

        private void Shoot()
        {
            var position = _shootProperties.bulletSpawnPoint.position;
            Quaternion rotation = _shootProperties.bulletSpawnPoint.rotation;
            ObjectPooling.Instant.GetGameObject(_shootProperties.muzzleFlash, position, rotation)
                .SetActive(true);
            ObjectPooling.Instant.GetGameObject(_shootProperties.bulletPrefab, position, rotation)
                .SetActive(true);
            
            IShootable shootable = _head.GetComponentInParent<IShootable>();
            if(shootable != null)
                shootable.Shoot();
        }
    }
}