﻿using Behaviour_Trees;
using UnityEngine;

namespace Nodes
{
    public class RangeNode : Node
    {
        private readonly float _range;
        private readonly Transform _target;
        private readonly Transform _origin;
        
        public RangeNode(Transform origin, Transform target, float range)
        {
            _range = range;
            _target = target;
            _origin = origin;
        }


        public override NodeState Evaluate()
        {
            float distance = Vector3.Distance(_origin.position, _target.position);
            return distance <= _range ? NodeState.Success : NodeState.Failure;
        }
    }
}