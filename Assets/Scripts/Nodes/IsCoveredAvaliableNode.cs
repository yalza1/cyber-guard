﻿using Behaviour_Trees;
using Enemies;
using UnityEngine;

namespace Nodes
{
    public class IsCoveredAvaliableNode : Node
    {
        private readonly Cover[] _availableCovers;
        private readonly Transform _target;
        private readonly EnemyAI _enemyAI;
        
        public IsCoveredAvaliableNode(Cover[] availableCovers, Transform target, EnemyAI enemyAI)
        {
            _availableCovers = availableCovers;
            _target = target;
            _enemyAI = enemyAI;
        }
        public override NodeState Evaluate()
        {
            Transform bestCoverSpot = FindBestCoverSpot();
            _enemyAI.SetBestCoverSpot(bestCoverSpot);
            return bestCoverSpot != null ? NodeState.Success : NodeState.Failure;
        }
        
        private  Transform FindBestCoverSpot()
        {
            float minAngle = 90;
            Transform bestCoverSpot = null;
            foreach (var spot in _availableCovers)
            {
                Transform bestSpotInCover = FindBestSpotInCover(spot, ref minAngle);
                if (bestSpotInCover != null)
                {
                    bestCoverSpot = bestSpotInCover;
                }
            }
            return bestCoverSpot;
        }
        
        private Transform FindBestSpotInCover(Cover cover,ref float minAngle)
        {
            Transform[] spots = cover.CoverSpots;
            Transform bestSpot = null;
            foreach (var spot in spots)
            {
                Vector3 direction = _target.position - spot.position;
                if (CheckIfSpotIsAvaliable(spot))
                {
                    float angle = Vector3.Angle(spot.forward, direction);
                    if (angle < minAngle)
                    {
                        minAngle = angle;
                        bestSpot = spot;
                    }
                }
            }

            return bestSpot;
        }
        
        private bool CheckIfSpotIsAvaliable(Transform spot)
        {
            if(Physics.Raycast(spot.position, _target.position - spot.position, out var hit))
            {
                if (hit.transform != _target)
                {
                    return true;
                }
            }

            return false;
        }
     
    }
}