﻿using Behaviour_Trees;
using UnityEngine;
using UnityEngine.AI;

namespace Nodes
{
    public class MeleeAttackNode : Node
    {
        private readonly Transform _target;
        private readonly NavMeshAgent _agent;
        private readonly Transform _origin;
        private readonly Animator _animator;

        private readonly float _delayAttack = 3f;
        private float _timmer;
        private static readonly int Attack1 = Animator.StringToHash("Attack1");
        private static readonly int Attack2 = Animator.StringToHash("Attack2");

        public MeleeAttackNode(Transform origin,Transform target, NavMeshAgent agent)
        {
            _origin = origin;
            _target = target;
            _agent = agent;
            _animator = agent.GetComponent<Animator>();
        }

        public override NodeState Evaluate()
        {
            var position = _target.position;
            _origin.LookAt(new Vector3(position.x, _origin.position.y, position.z));
            
            _animator.SetBool("Running",false);
            _agent.isStopped = true;
            
            if (_timmer > _delayAttack)
            {
                int random = Random.Range(1, 3);
                if(random == 1)
                    _animator.SetTrigger(Attack1);
                else
                    _animator.SetTrigger(Attack2);
                _timmer = 0f;
            }   
            _timmer += Time.deltaTime;
            
            
            return NodeState.Running;
        }
    }
}